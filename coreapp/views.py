from coreapp import app
from flask import request,abort
from datetime import date

@app.route('/')
def index():

    page = '''
	<DOCTYPE! html>
	<html lang="en-US">
	<head>
	<title>Find date</title>
	<meta charset="utf-8"/>
	</head>
	<body>
	<h1>Find the date of the week</h1>
	<p>select "day" "month" "year", summit to get the date of the week.<p>
	<form action="/dow" method="get">
		<select name = "day" id = "day">
			<option value= 1> 1</option>
			<option value= 2> 2</option>
			<option value= 3> 3</option>
			<option value= 4> 4</option>
			<option value= 5> 5</option>
			<option value= 6> 6</option>
			<option value= 7> 7</option>
			<option value= 8> 8</option>
			<option value= 9> 9</option>
			<option value= 10> 10</option>
			<option value= 11> 11</option>
			<option value= 12> 12</option>
			<option value= 13> 13</option>
			<option value= 14> 14</option>
			<option value= 15> 15</option>
			<option value= 16> 16</option>
			<option value= 17> 17</option>
			<option value= 18> 18</option>
			<option value= 19> 19</option>
			<option value= 20> 20</option>
			<option value= 21> 21</option>
			<option value= 22> 22</option>
			<option value= 23> 23</option>
			<option value= 24> 24</option>
			<option value= 25> 25</option>
			<option value= 26> 26</option>
			<option value= 27> 27</option>
			<option value= 28> 28</option>
			<option value= 29> 29</option>
			<option value= 30> 30</option>
			<option value= 31> 31</option>
		</select>
		<select name = "month" id = "month">
			<option value= 1> 1</option>
			<option value= 2> 2</option>
			<option value= 3> 3</option>
			<option value= 4> 4</option>
			<option value= 5> 5</option>
			<option value= 6> 6</option>
			<option value= 7> 7</option>
			<option value= 8> 8</option>
			<option value= 9> 9</option>
			<option value= 10> 10</option>
			<option value= 11> 11</option>
			<option value= 12> 12</option>
		</select>
		<select name = "year" id = "year">
			<option value= 2014> 2014</option>
			<option value= 2015> 2015</option>
			<option value= 2016> 2016</option>
			<option value= 2017> 2017</option>
			<option value= 2018> 2018</option>
			<option value= 2019> 2019</option>
			<option value= 2020> 2020</option>
		</select>
		<input value="submit" id="submit" type="submit"/> 
	</form>
	</body>
	</html>

	'''

    return page
	
@app.route('/dow')
def dow():
    page = '''
	<DOCTYPE! html>
	<html lang="en-US">
	<head>
	<title>Find date</title>
	<meta charset="utf-8"/>
	</head>
	<body>
	<h1>Day of the week</h1>
	<p>{0}<p>

	</body>
	</html>

	'''
    mydict = request.args
    print(type(mydict))
    keys = mydict.keys()
    response_str = ""
    if len(keys)<3:
        response_str = "Error: missing, incomplete, or incorrect input."
    else:
        weekList = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        day = int(request.args.get('day'))
        month = int(request.args.get('month'))
        year = int(request.args.get('year'))
        try:
            dates = date(year,month,day).weekday()
            response_str = weekList[dates]
        except Exception as e:
            error_msg = str(e)
            response_str = error_msg
    return page.format(response_str)